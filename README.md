## MereLinux ##

A place to post mere-linux pacman templates for additional packages and share them with the mere-linux community

**Site: **   https://merelinux.org/
**Source:**  https://github.com/jhuntwork/merelinux/

The core base of the system is complete, although many building blocks for building more packages are needed, so I am expanding the base by adding a few of my own.
I may be working on several ones, the ones that have been built and are utilized without problems I am sharing here.

For those who don't know about merelinux it is posix based and built against **musl**, it incorporates **busybox** as utilities and alternative init, it also includes most of **skarnet**'s **s6** software for init and service management.

The package manager is **pacman** from ArchLinux.
Upstream packages are split in their elements and not grouped as one.

There are **two repositories** of ready built packages core and testing but at this momment most of what is in testing has also passed to core as stable.

Someone with experience with pacman from other distros will know what to do with the following:


## mere linux ##

[testing]

Server = http://pkgs.merelinux.org/testing

[core]

Server = http://pkgs.merelinux.org/core


